/*
 * @Author Andrew Greaves
 * Class that implements the board game
 */
public class BoardGame {

	private int board_size, empty_positions, max_levels;
	private char[][] gameBoard;

	//Constructor
	public BoardGame(int board_size, int empty_positions, int max_levels) {
		this.board_size = board_size;
		this.empty_positions = empty_positions;
		this.max_levels = max_levels;
		this.gameBoard = new char[board_size][board_size];
		for (int i = 0; i < board_size; i++) {
			for (int j = 0; j < board_size; j++) {
				gameBoard[i][j] = 'g';
			}
		}
	}

	/*
	 * Creates a hash dictionary
	 * @return a new hash dictionary
	 */
	public HashDictionary makeDictionary() {
		return new HashDictionary(9887);
	}

	/*
	 * Returns the score of the board configuration if it is in the dictionary, -1 otherwise
	 */
	public int isRepeatedConfig(HashDictionary dict) {
		String boardConfig = "";
		for (int i = 0; i < board_size; i++) {
			for (int j = 0; j < board_size; j++) {
				boardConfig = boardConfig + gameBoard[i][j];
			}
		}
		int score = dict.getScore(boardConfig);
		if (score == -1)
			return -1;
		else
			return score;
	}

	/*
	 * Puts a new configuration into the hash dictionary based on the current game board
	 */
	public void putConfig(HashDictionary dict, int score) {
		String boardConfig = "";
		for (int i = 0; i < board_size; i++) {
			for (int j = 0; j < board_size; j++) {
				boardConfig = boardConfig + gameBoard[i][j];
			}
		}
		Configuration config = new Configuration(boardConfig, score);
		dict.put(config);
	}

	/*
	 * Save the play of either the human or computer
	 */
	public void savePlay(int row, int col, char symbol) {
		gameBoard[row][col] = symbol;
	}

	/*
	 * Checks whether a given position is empty 
	 */
	public boolean positionIsEmpty(int row, int col) {
		if (gameBoard[row][col] == 'g')
			return true;
		else
			return false;
	}

	/*
	 * Checks whether a given tile was placed by the computer
	 */
	public boolean tileOfComputer(int row, int col) {
		if (gameBoard[row][col] == 'o')
			return true;
		else
			return false;
	}

	/*
	 * Checks whether a given tile was placed by the human
	 */
	public boolean tileOfHuman(int row, int col) {
		if (gameBoard[row][col] == 'b')
			return true;
		else
			return false;
	}

	/*
	 * Checks whether there are n adjacent symbols in the same row, column or diagonal 
	 */
	public boolean wins(char symbol) {
		for (int row = 0; row < board_size; row++) {
			if (checkRow(row, symbol))
				return true;
		}
		for (int col = 0; col < board_size; col++) {
			if (checkColumn(col, symbol))
				return true;
		}
		if (checkRightDiag(symbol))
			return true;

		else if (checkLeftDiag(symbol))
			return true;

		return false;
	}

	/*
	 * Checks if the game is a draw
	 */
	public boolean isDraw(char symbol, int empty_positions) {
		if ((empty_positions == 0) && (wins(symbol) == false) && (emptySpace() == 0))
			return true;
		else if ((empty_positions > 0) && (emptySpace()==empty_positions) && (adjacent(symbol) == false) && (wins(symbol) == false))
			return true;
		return false;
	}

	/*
	 * Evaluates the board to determine the best move for the computer
	 */
	public int evalBoard(char symbol, int empty_positions) {
		if (wins('o'))
			return 3;
		else if (wins('b'))
			return 0;
		else if (isDraw(symbol, empty_positions))
			return 2;
		else
			return 1;
	}

	/*
	 * private method to determine if there are n adjacent symbols in the same row
	 */
	private boolean checkRow(int row, char symbol) {
		int numOccurences = 0, col = 0;
		for (int currentCol = col; currentCol < board_size; currentCol++) {
			if (gameBoard[row][currentCol] == symbol)
				numOccurences++;
		}
		return (numOccurences == board_size);
	}

	/*
	 * private method to determine if there are n adjacent symbols in the same column */
	private boolean checkColumn(int col, char symbol) {
		int numOccurences = 0, row = 0;
		for (int currentRow = row; currentRow < board_size; currentRow++) {
			if (gameBoard[currentRow][col] == symbol)
				numOccurences++;
		}
		return (numOccurences == board_size);
	}

	/*
	 * private method to determine if there are n adjacent symbols in right diagonal
	 */
	private boolean checkRightDiag(char symbol) {
		int numOccurences = 0, row = 0, col = 0;
		while (row < board_size) {
			if (gameBoard[row][col] == symbol) {
				numOccurences++;
			}
			row++;
			col++;
		}
		return (numOccurences == board_size);
	}

	/*
	 * private method to determine if there are n adjacent symbols in the left diagonal
	 */
	private boolean checkLeftDiag(char symbol) {
		int numOccurences = 0, row = 0, col = board_size - 1;
		while (row < board_size) {
			if (gameBoard[row][col] == symbol) {
				numOccurences++;
			}
			row++;
			col--;
		}
		return (numOccurences == board_size);
	}

	/*
	 * private method to determine if an empty tile has any specified symbols adjacent to it
	 */
	private boolean adjacent(char symbol) {
		for (int i = 0; i < board_size; i++) {
			for (int j = 0; j < board_size; j++) {
				if (gameBoard[i][j] == 'g') {
					if ((i + 1 < board_size && gameBoard[i + 1][j] == symbol)
							|| (j + 1 < board_size && gameBoard[i][j + 1] == symbol)
							|| (i + 1 < board_size && j + 1 < board_size && gameBoard[i + 1][j + 1] == symbol)
							|| (i - 1 >= 0 && gameBoard[i - 1][j] == symbol)
							|| (j - 1 >= 0 && gameBoard[i][j - 1] == symbol)
							|| (i + 1 < board_size && j - 1 >= 0 && gameBoard[i + 1][j - 1] == symbol)
							|| (i - 1 >= 0 && j + 1 < board_size && gameBoard[i - 1][j + 1] == symbol)
							|| (i - 1 >= 0 && j - 1 >= 0 && gameBoard[i - 1][j - 1] == symbol))
						return true;
				}
			}
		}
		return false;
	}

	/*
	 * private method to determine the number of empty spaces on the game board
	 */
	private int emptySpace() {
		int empty=0;
		for (int i = 0; i < board_size; i++) {
			for (int j = 0; j < board_size; j++) {
				if(positionIsEmpty(i,j))
					empty++;
			}
		}
		return empty;
	}

}
