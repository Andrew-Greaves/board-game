/*
 * @Author Andrew Greaves
 * Class which implements the configurations which are put into hash dictionary
 */
public class Configuration {
	
	private String config;
	private int score;
	
	//Constructor
	public Configuration(String config, int score) {
		this.config=config;
		this.score=score;
	}

	//Returns a string configuration
	public String getStringConfiguration() {
		return config;
	}

	//Returns score associated with a configuration
	public int getScore() {
		return score;
	}
	
	
}
