/*
 * @author Andrew Greaves
 * Class that implements a hash table using separate chaining, implementing DictionaryADT
 */
import java.util.LinkedList;

public class HashDictionary implements DictionaryADT {
	private LinkedList<Node<Configuration>>[] dictionary;
	
	//Constructor
	public HashDictionary(int size) {
		this.dictionary=new LinkedList[size];
		for(int i=0;i<dictionary.length;i++) {
			if(dictionary[i]==null){
				dictionary[i]=new LinkedList<Node<Configuration>>();
			}
		}
	}

	//Interface Methods
	/*
	 * Method to put a configuration into the hash table
	 * @return 1 if a collision occurs, 0 if not
	 * @see DictionaryADT#put(Configuration)
	 */
	public int put(Configuration data) throws DictionaryException {
		if(get(data.getStringConfiguration())==true) throw new DictionaryException("Configuration is already in dictionary");
		int pos=hashFunction(data.getStringConfiguration());
		LinkedList<Node<Configuration>> list=dictionary[pos];
		Node<Configuration>node=new Node<Configuration>(data);
		if(list.size()>0)
			node.setNext(list.getFirst());
		dictionary[pos].addFirst(node);
		if(dictionary[pos].size()-1>0)
			return 1;
		return 0;
	}

	/*
	 * Removes a configuration from the hash table
	 * @see DictionaryADT#remove(java.lang.String)
	 */
	public void remove(String config) throws DictionaryException {
		if(get(config)==false) throw new DictionaryException("Configuration is not in dictionary");
		else {
			LinkedList<Node<Configuration>> list=dictionary[hashFunction(config)];
			Node<Configuration>node=list.getFirst();
			while((node.getElement()!=null) && (node.getElement().getStringConfiguration().equals(config)==false)){
				node=node.getNext();
			}
			if(node.getElement().getStringConfiguration().equals(config))
				list.remove(node.getElement());
		}
	}

	/*
	 * Returns the score associated with a certain string configuration
	 * @return -1 if configuration is not in hash table, returns associated score if it is
	 * @see DictionaryADT#getScore(java.lang.String)
	 */
	public int getScore(String config) {
		if (get(config)==false) return -1;
		else {
			LinkedList<Node<Configuration>> list=dictionary[hashFunction(config)];
			Node<Configuration>node=list.getFirst();
			while((node!=null) && (node.getElement().getStringConfiguration().equals(config)==false)){
				node=node.getNext();
			}
			return node.getElement().getScore();
		}
	}
	
	/*
	 * private function to determine where a configuration is stored in the hash table
	 * @return location in which a configuration will be stored
	 */
	private int hashFunction(String config) {
		int value,x=31;
		value=(int)config.charAt(config.length()-1);
		for (int i=config.length()-2;i>=0;i--) {
			value=(value*x+(int)config.charAt(i))%dictionary.length;
		}
		return value%dictionary.length;
	}
	
	/*
	 * private function to find a given configuration in the hash table
	 * @return true if configuration is in the hash table, false otherwise
	 */
	private boolean get(String config) {
		LinkedList<Node<Configuration>> list=dictionary[hashFunction(config)];
		if(list.size()==0) return false;
		Node<Configuration>node=list.getFirst();
		while((node!=null) && (node.getElement().getStringConfiguration().equals(config)==false)){
			node=node.getNext();
		}
		if (node==null)
			return false;
		else return true;
	}
	
	

}
